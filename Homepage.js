import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from "react-native";
import {StatusBar} from "expo-status-bar";
import {Button} from "react-native-web";
import Register from "./Register";

function Homepage(props) {
    return (
        <View style={styles.container}>
            <Text style={styles.homeTitle}>Proud Messenger</Text>
            <Text>{JSON.stringify(props.location)}</Text>
            <Button
                onPress={() => props.history.push('/Connexion')}
                title="S'inscrire/connexion"
                color= '#eba434'
            />

            <StatusBar style="auto" />
        </View>
    );

}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    homeTitle:{
        color: '#eba434',
        fontSize:'5em',
        fontFamily:'Architects Daughter-cursive'
    },

})

export default Homepage