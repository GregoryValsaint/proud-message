import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {StyleSheet, TextInput, Text, View} from "react-native";
import SafeAreaView, {Button, FlatList} from "react-native-web";
import {StatusBar} from "expo-status-bar";
import Register from "./Register";

function Message(props) {

    const [newMessage, setNewMessage] = useState("")
    const [messages, setMessages] = useState([])
    const [citycode, setCitycode] = useState("72100")
    const [category, setCategory] = useState([])
    const [idCat, setIdCat] = useState(null)
    const [idCurrentUser, setIdCurrentUser] = useState(null)
    const [currentUser, setCurrentUser] = useState(null)
    const [currentUsers, setCurrentUsers] = useState(null)

    const token = props.token
    const location = props.location



    useEffect(() => {setInterval(getMessage, 1000)}, [])
    useEffect(getCategorie,[])
    useEffect(() => {setInterval(getUser, 1000)}, [])
    useEffect(getUsers, [])




    function addMessage() {
        setMessages([...messages, newMessage])
        setNewMessage("")
    }

    // user

    function sendMessage() {

        axios.post("https://api.dunarr.com/api/messages",
            {
                "message": newMessage,
                "category": idCat,
                "citycode": citycode
            },
            {
                headers: {
                    "Authorization": "Bearer " + token
                }
            }
        ).then(response => {
            setIdCurrentUser(response.data)
        })
    }

    function getMessage() {
        axios.get("https://api.dunarr.com/api/messages",
            {
                headers: {
                    "Authorization": "Bearer " + token
                },
                params: {
                    "citycode": citycode,
                    "category": idCat,
                }
            }

        ).then(response => {
            setMessages(response.data.results);
            setIdCurrentUser(response.data.results[0].id)
            console.log(messages)
        })
    }



    function getCategorie() {
        axios.get("https://api.dunarr.com/api/categories",
            {
                headers: {
                    "Authorization": "Bearer " + token
                }
            }
        ).then(response => {
            setCategory(response.data.results);
        })

    }
    function getUsers(){
        axios.get("https://api.dunarr.com/api/users",
            {
                headers: {
                    "Authorization": "Bearer " + token
                }
            }
        ).then(response => {
            setCurrentUsers(response.data.results)}
        )
    }

    function getUser(){
        axios.get("https://api.dunarr.com/api/users/" + idCurrentUser,
            {
                headers: {
                    "Authorization": "Bearer " + token
                }
            }
        ).then(response => {setCurrentUser(response.data.results)}
        )
    }


    return (
    <View style={styles.container}>
        <Text>Conversation</Text>

        <TextInput
            style={{height: 40, borderColor: 'gray', borderWidth: 1}}
            onChangeText={setIdCat}
            value={idCat}
        />
        <TextInput
            multiline={true}
            numberOfLines={4}
            style={{height: 40, borderColor: 'gray', borderWidth: 1}}
            onChangeText={setNewMessage}
            value={newMessage}
        />
        <Button onPress={sendMessage} title="Envoyer"/>
        <Text>{JSON.stringify(props.location)}</Text>
        {/*messages.map( function(msg){
            return <Text>{msg.content}</Text>
        })*/}
        <FlatList
            data={messages}
            style={{}}
            renderItem={data => {return <Text>{data.item.content}</Text>}}
            keyExtractor={item => item.id}
        />

        <StatusBar style="auto"/>
    </View>
    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    }

})

export default Message