import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {StyleSheet, TextInput, Text, View} from "react-native";
import {Button} from "react-native-web";
import {StatusBar} from "expo-status-bar";
import * as Location from "expo-location";
import {useHistory} from "react-router-native";

function Register(props) {

    const [newUsername, setNewUsername] = useState("")
    const [newPassword, setnewPassword] = useState("")
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")

    const [citycode, setCitycode] = useState("")


    const history = useHistory();

    const setToken = props.setToken
    const location = props.location
    const latitude = props.setLatitude
    const longitude = props.setLongitude


    useEffect(getCitycode, [])



    // user

    function login() {

        axios.post("https://api.dunarr.com/api/login",
            {
                "username": username,
                "password": password
            }
        ).then(response => {
            setToken(response.data.token);
            history.push('/Messagerie')

        })
    }

    function register() {
            axios.post("https://api.dunarr.com/api/register",
                {
                    "username": newUsername,
                    "password": newPassword
                }
            ).then(function (){login()}
            )
    }

    function getCitycode() {
        axios.post("https://api-adresse.data.gouv.fr/reverse/?lon="+longitude+"&lat="+latitude,

        ).then(response => {
            setCitycode(response.data)})
        console.log(latitude)
    }


    return (
        <View style={styles.container}>

            <Text>S'inscrire</Text>
            <TextInput
                style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                onChangeText={setNewUsername}
                value={newUsername}
            />
            <TextInput
                style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                onChangeText={setnewPassword}
                value={newPassword}
            />
            <Button onPress={register} title="S'inscrire"/>


            <Text>Connexion</Text>
            <TextInput
                style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                onChangeText={setUsername}
                value={username}
            />
            <TextInput
                style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                onChangeText={setPassword}
                value={password}
            />
            <Button onPress={login} title="Connexion"/>
            <Text>{JSON.stringify(location)}</Text>


            <StatusBar style="auto" />
        </View>
    );

}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    }
})

export default Register