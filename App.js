import { StatusBar } from 'expo-status-bar';
import React, {useState,useEffect} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {NativeRouter, NavLink, Switch, Route} from "react-router-native";
import Homepage from "./Homepage"
import Register from "./Register";
import Message from "./Message";
import * as Location from "expo-location";

export default function App() {
  const [token, setToken] = useState ("")
  const [location, setLocation] = useState("")
  const [longitude, setLongitude] = useState("")
  const [latitude, setLatitude] = useState("")

  useEffect(geolocation,[])
  async function geolocation() {
    const {status} = await Location.requestPermissionsAsync()
    if (status !== "granted") {

    }
    const position = await Location.getCurrentPositionAsync()
    setLocation(position)
    setLongitude(position.coords.longitude)
    setLatitude(position.coords.latitude)
  }

  return (
    <View style={styles.container}>
      <NativeRouter>
        <Switch>
          <Route exact path="/" component={Homepage}/>
          <Route path="/Connexion"> <Register setToken={setToken} location={location} longitude={longitude} latitude={latitude}/></Route>
          <Route path="/Messagerie" ><Message token={token} location={location} longitude={longitude} latitude={latitude}/></Route>
        </Switch>
      </NativeRouter>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
